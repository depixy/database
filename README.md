# @depixy/database

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![NPM][npm_badge]][npm] [![semantic-release][semantic_release_badge]][semantic_release]

Database schema for Depixy.

## Installation

```
npm i @depixy/database
```

[license]: https://gitlab.com/depixy/database/blob/master/LICENSE
[license_badge]: https://img.shields.io/npm/l/@depixy/database
[pipelines]: https://gitlab.com/depixy/database/pipelines
[pipelines_badge]: https://gitlab.com/depixy/database/badges/master/pipeline.svg
[npm]: https://www.npmjs.com/package/@depixy/database
[npm_badge]: https://img.shields.io/npm/v/@depixy/database/latest.svg
[semantic_release]: https://github.com/semantic-release/semantic-release
[semantic_release_badge]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
