## [1.0.1](https://gitlab.com/depixy/database/compare/v1.0.0...v1.0.1) (2020-12-14)


### Bug Fixes

* update package.json ([48ac962](https://gitlab.com/depixy/database/commit/48ac96260b3f990b61d53a65531cff7cb59040af))

# 1.0.0 (2020-12-04)


### Features

* initial commit ([c8e1f0d](https://gitlab.com/depixy/database/commit/c8e1f0d686347d565fc4f24743a274d34b168010))
