DROP TABLE IF EXISTS "Role";
DROP TABLE IF EXISTS "Permission";
DROP TABLE IF EXISTS "_RolePermission";
DROP INDEX IF EXISTS "_RolePermission_AB_unique";
DROP INDEX IF EXISTS "_RolePermission_B_index";

DROP TABLE IF EXISTS "User";
DROP TABLE IF EXISTS "UserToken";

DROP TABLE IF EXISTS "TagCategory";
DROP TABLE IF EXISTS "Tag";
DROP TABLE IF EXISTS "TagAlias";
DROP TABLE IF EXISTS "_TagImplication";
DROP INDEX IF EXISTS "_TagImplication_AB_unique";
DROP INDEX IF EXISTS "_TagImplication_B_index";
DROP TABLE IF EXISTS "_TagSuggestion";
DROP INDEX IF EXISTS "_TagSuggestion_AB_unique";
DROP INDEX IF EXISTS "_TagSuggestion_B_index";

DROP TABLE IF EXISTS "GalleryCategory";
DROP TABLE IF EXISTS "Gallery";
DROP TABLE IF EXISTS "_GalleryTag";
DROP INDEX IF EXISTS "_GalleryTag_AB_unique";
DROP INDEX IF EXISTS "_TagSugges_GalleryTag_B_indextion_B_index";
DROP TABLE IF EXISTS "Image";
