import knex from "knex";
import path from "path";

export const createClient = (connection: string): knex =>
  knex({
    client: "pg",
    connection,
    migrations: {
      tableName: "migration",
      directory: path.join(__dirname, "migration"),
      loadExtensions: [".js"],
      extension: "js"
    }
  });
