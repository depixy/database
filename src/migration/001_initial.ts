import Knex from "knex";
import { readFile } from "fs/promises";
import path from "path";

const migrationDir = path.join(__dirname, "..", "..", "migration");

export async function up(knex: Knex): Promise<void> {
  const query = await readFile(path.join(migrationDir, "V1__initial.sql"), {
    encoding: "utf-8"
  });
  await knex.raw(query);
}

export async function down(knex: Knex): Promise<void> {
  const query = await readFile(path.join(migrationDir, "U1__initial.sql"), {
    encoding: "utf-8"
  });
  await knex.raw(query);
}
